#!/bin/bash

condactivate

#this script accepts illumina standard fastq names
R1=$1
reference="hg38"
#flowcell=$3 
#ref_genome=/lustre1/genomes/hg19/bwa/hg19
ref_genome=/lustre1/genomes/${reference}/bwa/${reference}
fasta_genome=/lustre1/genomes/${reference}/fa/${reference}.fa
#if [ ! -f ${ref_genome}.fa ]; then
#echo "$reference not found"
#exit
#fi

#File=$R1

BWAV=$(bwa 2>&1|grep Version|cut -d: -f2)
SAMV=$(samtools --version 2>&1|grep samtools)

flowcell=$(zcat $R1|head -n 1|cut -d: -f 3)
ID=`echo $R1| (IFS=_; while read level1 level2 level3 level4 ;do echo $level1; done)`
sample=`echo $R1| (IFS=_; while read level1 level2 level3 level4 ;do echo $level2; done)`
lane=`echo $R1| (IFS=_; while read level1 level2 level3 level4 ;do echo $level3; done)`
name=${ID}.${flowcell}.${lane}

BAM_DIR=${SPLITTED_BAM_DIR}/${samplename}

echo "$name"
echo "$ref_genome"

user=`whoami`

R2=${R1/_R1_/_R2_}
 

script_name=${ID}
CWD=`pwd`

cat <<__EOF__> ${script_name}

#PBS -N ${ID}.${sample}
#PBS -l select=1:app=java:ncpus=4:mem=18gb
#PBS -P 892 
#PBS -q workq
#PBS -m ae


cd $CWD 

condactivate

######## Alignment #########

bwa mem -t 2 -R "@RG\tID:$name\tPL:illumina\tPU:$lane\tSM:$ID\tLB:$name\tCN:CTGB\tPN:bwa $BWAV $SAMV\tSO:coordinate" $ref_genome  $R1 $R2 | samtools view -Sb - | samtools sort - -T $CWD/${name}_${reference}.tmp -o $CWD/${name}_${reference}_sorted.bam


__EOF__

 
qsub $script_name

