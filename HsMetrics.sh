#!/bin/bash

R1=$1	#BAM
dict=/lustre1/genomes/hg38/fa/hg38.fa
baits=/lustre1/genomes/hg38/annotation/exome_targets_liftover/agilent_v7_sureselect_MergedProbes.interval_list
target=/lustre1/genomes/hg38/annotation/exome_targets_liftover/agilent_v7_sureselect_Regions.interval_list
name=${R1%%_*}

user=`whoami`

condactivate
 
PWD=`pwd`

script_name=${name}_HS


cat <<__EOF__> $script_name

#!/bin/bash
#PBS -N ${name}
#PBS -l select=1:ncpus=1:mem=16g:app=java
#PBS -V
#PBS -q workq
#PBS -m ae


cd $PWD
ulimit -l unlimited
ulimit -s unlimited

condactivate

export  _JAVA_OPTIONS="-Xmx16g -Djava.io.tmpdir=/lustre2/scratch/tmp"

picard CollectHsMetrics \
    BAIT_SET_NAME="agilent_v7_sureselect_Regions_hg38" \
    BI=$baits \
    TI=$target \
    I=$R1 \
    O=${name}.hsmetrics \
    REFERENCE_SEQUENCE=$dict \
    PER_TARGET_COVERAGE=${name}_TARGET_COVERAGE.txt \
    VALIDATION_STRINGENCY=SILENT

__EOF__

qsub ${script_name}
