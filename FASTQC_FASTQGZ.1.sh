bamFile=$1
script_name=${bamFile%%_*}.sh
currentDirectory=`pwd`
#bamFiles=`ls ${bamFile%%_*}*fastq.gz|tee`
A=$(ls ${bamFile}*fastq.gz)
cat <<__EOF__> $script_name

#!/bin/bash
#PBS -N FASTQC
#PBS -l select=1:ncpus=6:mem=6g:app=java
#PBS -V
#PBS -Wsandbox=private
#PBS -q workq
##PBS -M bucci.gabriele@hsr.it
#PBS -m ae

condactivate

cd ${currentDirectory}
printf '%s ' ${A}
fastqc --casava --noextract -q  -t 4 -a /lustre1/genomes/Illumina_Adapters/Adapters.tab $(printf '%s ' ${A})

condeactivate

__EOF__

qsub $script_name

#for i in `ls *R1_001.fastq.gz`; do /lustre2/scratch/Ghia_433_ERIC_panel_CLL/ghia_433_eric_panel_cll/FASTQC_FASTQGZ.sh $i; done