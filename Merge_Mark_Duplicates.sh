#!/bin/bash
bamFile=$1 #sample name (ls *_S?_L00?_R1_00?.fastq.gz|cut -d_ -f 1|sort -u > Names.txt)


A=$(ls $bamFile*bam); echo $(printf 'INPUT=%s ' ${A}) 
##########SORTED BY COORDINATE#####

script_name=${bamFile}.sh
currentDirectory=`pwd`
SORTED_BAM_DIR=${currentDirectory}
BAM_DIR=${currentDirectory}/BAM
mkdir -p $BAM_DIR


cat <<__EOF__> $script_name

#!/bin/bash
#PBS -N M.${script_name:0:5}
#PBS -l select=1:ncpus=4:mem=32g:app=java
#PBS -V
#PBS -Wsandbox=private
#PBS -q workq
##PBS -M bucci.gabriele@hsr.it

rm /tmp/gbucci/*

condactivate

cd $currentDirectory

######## In only one step I merge bam file and mark duplicates
picard MarkDuplicates \
$(printf 'INPUT=%s ' ${A})  \
OUTPUT=${BAM_DIR}/${bamFile}_dedup.bam \
METRICS_FILE=${BAM_DIR}/${bamFile}_metrics \
REMOVE_DUPLICATES=false \
ASSUME_SORTED=true \
CREATE_INDEX=true

condeactivate
__EOF__


qsub $script_name
