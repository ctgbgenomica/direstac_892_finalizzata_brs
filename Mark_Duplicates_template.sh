#!/bin/bash
bamFile=$1 #sample name (ls *_S?_L00?_R1_00?.fastq.gz|cut -d_ -f 1|sort -u > Names.txt)


A=$(ls $bamFile*bam); echo $(printf 'INPUT=%s ' ${A}) 
##########SORTED BY COORDINATE#####

script_name=${bamFile}.sh
currentDirectory=`pwd`
SORTED_BAM_DIR=${currentDirectory}
BAM_DIR=${currentDirectory}/BAM
mkdir -p $BAM_DIR


cat <<__EOF__> $script_name

#!/bin/bash
#PBS -N M.${script_name:0:5}
#PBS -l select=1:ncpus=4:mem=32g:app=java
#PBS -V
#PBS -Wsandbox=private
#PBS -q workq
##PBS -M bucci.gabriele@hsr.it

rm /tmp/gbucci/*

cd $currentDirectory

######## In only one step I merge bam file and mark duplicates

######## In only one step I merge bam file and mark duplicates
java -Xmx16g -Djava.io.tmpdir=/lustre2/scratch/gbucci/tmp -jar /usr/local/cluster/bin/picard.jar MarkDuplicates \
$(printf 'INPUT=%s ' ${mv}/*RG.bam)  \
OUTPUT=${BAM_DIR}/${myname}_RGdedup.bam \
METRICS_FILE=${BAM_DIR}/${myname}_metrics \
REMOVE_DUPLICATES=true \
ASSUME_SORTED=true \
CREATE_INDEX=true

java -Xmx16g -Djava.io.tmpdir=/lustre2/scratch/gbucci/tmp -jar /usr/local/cluster/bin/picard.jar MarkDuplicates \
INPUT=$currentDirectory \
INPUT=/lustre1/workspace/Ciceri/161_Leukemia/170630_SN859_0453_AHNJKFBCXY/FASTQ/SORTED/ALFE1_S1_001_hs38DH_1.sorted.bam  \
OUTPUT=${BAM_DIR}/${bamFile}_RGdedup.bam \
METRICS_FILE=${BAM_DIR}/${bamFile}_metrics \
REMOVE_DUPLICATES=false \
ASSUME_SORTED=true \
CREATE_INDEX=true

# java -Xmx32g  -jar /usr/local/cluster/bin/picard.jar BuildBamIndex INPUT=${BAM_DIR}/${myname}_RGdedup.bam OUTPUT=${BAM_DIR}/${myname}_RGdedup.bai


__EOF__


qsub $script_name
